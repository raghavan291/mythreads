COMPILE=gcc
CFLAGS=-c
CFLAGS+=-g

AR=ar
LINK=-L. mythread.a -static

all: clean mythread test

mythread:
	${COMPILE} ${CFLAGS} futex.c mythread.c list.c queue.c
	${AR} rcs mythread.a list.o futex.o mythread.o queue.o

test:
	${COMPILE} mythread_test.c ${LINK} -o test -g

clean:
	rm -rf *.a *.o test
