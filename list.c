/* Single Author info: 
 * rnedunc Raghavendran Nedunchezhian
 * Group info:
 * lbalaku Lokheshvar Balakumar
 * rnedunc Raghavendran Nedunchezhian
 * nramanj Navya Ramanjulu */

#include<stdio.h>
#include<stdlib.h>
#include "queue.h"
#include "list.h"
#include "mythread.h"

/*This is used to search for a particular node holding thread specific data*/
void *search_lnode(lnode* start, mythread_t tcb)
{
        lnode* current;
        current = start;
        while(current != NULL)
        {
                if(current->thread_control_block.tid == tcb.tid)
                        return (void *)current->thread_value;
                current = current->link;
        }
        /*when current node = NULL, all the elements in the list have been 
        * checked and the id is not found.*/
        return NULL;
}

/*This is used to delete a node hold thread specific data*/
int delete_lnode(lnode* start, mythread_t tcb)
{
	lnode* previous = NULL;
	lnode* current;
	
	while(current != NULL)
	{
		if(current->thread_control_block.tid == tcb.tid)
		{
			/*if the node to be deleted is the 1st node, free it*/
			if(current == start)
			{
				start = current->link;
				free(current);
				return 0;
			}
			else
			{
				previous->link = current->link;
				free(current);
				return 0;
			}
		}
		else
		{
			previous = current;
			current = current->link;
		}
	}
	/*if mode not found*/
	//printf("\nEINVAL: key value is invalid\n");
	return -1;
}

/*This is used to create a new node*/
lnode* create_lnode(mythread_t tcb, const void *value)
{
        lnode* new_lnode;
	if((new_lnode = (struct list_node*) malloc(sizeof(struct list_node))) == NULL)
        {
                printf("\ncreate_lnode - ENOMEM: Insufficient memory exists to associate the value with the key\n");
                return NULL;
        }

	//assign the values for list data	
	new_lnode->thread_control_block = tcb;
	new_lnode->thread_value = value;
	//printf("\n new_lnode : %d %d", new_lnode->thread_control_block.tid, tcb.tid);
	new_lnode->link = NULL;
	return new_lnode;
}

/*This is used to append a new node to the thread specific data*/
int append_lnode(lnode* start, lnode* new_node,  mythread_t tcb, const void *value)
{
	lnode* previous=NULL;
	lnode* temp;
	if(start == NULL)
	{
		new_node->link = NULL;
		start = new_node;
	}
	else
	{
		temp = start;
		while(temp != NULL)
		{
 			if(temp->thread_control_block.tid == tcb.tid)
                	{
      		        	temp->thread_value = value;
                        	free(new_node);
                        	return 1;
                	}
			previous = temp;
			temp = temp->link;
		}
		previous->link = new_node;
		new_node->link = NULL;
	}
	return 0;
}

