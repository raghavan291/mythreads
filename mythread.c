/*Single Author info: 
rnedunc Raghavendran Nedunchezhian
Group info:
lbalaku Lokheshvar Balakumar
rnedunc Raghavendran Nedunchezhian
nramanj	Navya Ramanjulu*/

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sched.h>
#include <stdio.h>
#include "futex.h"
#include "queue.h"
#include "list.h"

#define FIBER_STACK 4096*64

int futex_var=0;
static int total_threads=-2;
static int isfirsttime=0;
static int isfirsttime2=0;
static int ismainthreadcreated=0;
struct futex f;
struct futex block_main_futex;
struct futex block_exitthread_futex;
int haveyielded=0;
node *headofjobqueue;
struct futex yield_futex;

#define MYTHREAD_KEYS_MAX 100 

mythread_key_t tsd_key1 = 0;
mythread_key_t tsd_key2 = 0;

/* Counter to assign thread-specific data key*/
int tsd_key_counter=0;

/* Structure to store thread-specific data(TSD) key and a pointer to its values*/
struct TSD{
        mythread_key_t key;
        struct list_node* ptr;
};

/* Global array to store thread-specific data keys visible to all threads in the process. */
struct TSD globalTSD[MYTHREAD_KEYS_MAX];

struct wrapper_arguments
{
	void *(*functionname) (void *);
	void *functionarguments;
	mythread_t *tcb;
};

int  mythread_wrapper(void * arg);
void createmainthread();
void * myownmain(void * arg);
void * threadfunction2 (void * arg);

int mythread_join(mythread_t targetthread, void **status)
{
	int isinqueue;
	int jointargetcheck=1;

	queue_info->head->thread->join_target=targetthread.myaddress;
	isinqueue=findthread(targetthread.myaddress);

	if(isinqueue==0)// target thread is not present in the execution queue
		return 0;
	else // if the target thread is in the execution queue the yield the current thread
	{
		queue_info->head->thread->join_target=targetthread.myaddress; // add the target thread to the target thread field in the current thread and yield it
	

		while(jointargetcheck==1)
		{
			if(queue_info->head->thread->tid==0)
			{
				mythread_yield();
		//futex_down(&(queue_info->head->thread->thread_futex));//locking its own futex rather than that of the previous ones
		//futex_down(&yield_futex);
				jointargetcheck=findthread(queue_info->head->thread->join_target);
			}
			else
			{
				mythread_yield();// check the condition if this is has target yet to be done and keep it in loop
				 jointargetcheck=findthread(queue_info->head->thread->join_target);
				//break;
			}
		}		

		return 0;
	}
}

void mythread_exit(void *retval) 
{
	int status;
	node * tempnode = (node *) malloc (sizeof(node));
	if(queue_info->head->thread->tid==0) // if the exit is called by main then yield the main thread until the all the other threads are done
	{		
		while(total_threads>1)
		{
			mythread_yield();
		}	
		exit(0); // when all the threads are done then exit out of the while loop and end the program
	}
	else // if mythread_exit was called by a child thread then just put calling thread out of the queue without modifying the other threads
	{
		tempnode = queue_info->head;
		queue_info->head=queue_info->head->right;
		futex_up(&(queue_info->head->thread->thread_futex)); 
       		futex_up(&yield_futex);
		total_threads=total_threads-1;
		futex_down(&block_exitthread_futex);
	}
}


mythread_t mythread_self()
{
	return *(queue_info->head->thread);
}


int mythread_yield()
{
	int i;
	node * temporarynode = (node * ) malloc (sizeof(node));
	// up the futex of the executig thread and then down the futex of the last thread in the list 
	//that has already set its futex down so that the current thread will block and the next thread in the queue will start executing

	//futex_up(&block_main_futex);
	//futex_up(&(queue_info->head->thread->thread_futex));
	void * callingaddress;
	callingaddress=__builtin_return_address(1);

	haveyielded=haveyielded+1;
	temporarynode=queue_info->head;
	queue_info->head=queue_info->head->right;
	temporarynode->left=queue_info->tail;

	if(queue_info->head!=NULL)
	{
		queue_info->head->left=NULL;
	}
	if(queue_info->tail!=NULL)
	{
		temporarynode->right=queue_info->tail->right;
	}
	queue_info->tail->right=temporarynode;
	queue_info->tail=temporarynode;
	futex_up(&(queue_info->head->thread->thread_futex));
	futex_up(&yield_futex);
	futex_down(&(temporarynode->thread->thread_futex));
	//futex_down(&(temporarynode->thread->thread_futex));
	//futex_down(&(queue_info->tail->left->thread->thread_futex));
	haveyielded=haveyielded-1;
	return 0;
}
 


void * idlethreadfunction(void *a)
{
	struct mythread_t * idlethread=(struct mythread_t *) malloc(sizeof(mythread_t));
	int i=0;
	while(1)
	{
		i=mythread_yield();
	}
}

void * nestedthread(void * arg)
{
	struct mythread_t * idlethread=(struct mythread_t *) malloc(sizeof(mythread_t));
	int i=0;
}


void initialize_mythread_package()
{

	char print_queue[20]="queue initialized\n";
	void *stack;
	void *arg;
	int piddd;
	struct wrapper_arguments * wrapperarguments;

	stack = malloc( FIBER_STACK );
	if ( stack == 0 )
        {
                perror( "malloc: could not allocate stack" );
                exit( 1 );
         }

	arg=(void *) malloc(sizeof(void));

	mythread_t *idlethread=(mythread_t *) malloc (sizeof(mythread_t)); 
	init_queue_main();


	// create the main thread block and put it in the queue
	futex_init(&yield_futex,0);
	futex_init(&block_exitthread_futex,0);
	createmainthread();

	
	//create the idle thread
       	 futex_init(&f,0);
	 idlethread->mystack = stack;
         idlethread->myfunction =&idlethreadfunction;
         idlethread->myarguments=arg;
	 idlethread->join_target=NULL; 
	 wrapperarguments = (struct wrapper_arguments *) malloc (sizeof(struct wrapper_arguments));
         wrapperarguments->functionname=&idlethreadfunction;
         wrapperarguments->functionarguments=arg;
         wrapperarguments->tcb=idlethread;
	 futex_init(&(wrapperarguments->tcb->thread_futex),0);
        
	 void *wrapperarg = (void *) wrapperarguments;
	
 	 idlethread->tid = clone(&mythread_wrapper, (char*) stack + FIBER_STACK,
                SIGCHLD | CLONE_FS | CLONE_FILES | CLONE_SIGHAND | CLONE_VM,wrapperarg);

	futex_down(&f);
}


int  mythread_wrapper(void * arg)
{
	void * fromthreadfunction ;
	node * thread_block;
	int temp;
	node * temptail,*temphead;
	int *i = (int *) malloc (sizeof(int));
	char buffer[10]="wrapper";
	void ** status;
	struct wrapper_arguments * arguments = (struct wrapper_arguments *) arg;
	void * (*functionpointer) (void *) = *(arguments->functionname);
	int jointargetcheck=0;

	/* creating a node and insertig the TCB into the queue */
        total_threads+=1;
	temptail=(node *)malloc(sizeof(node));
	temphead=(node *)malloc(sizeof(node));
	thread_block=createNode();
	
	thread_block->thread=arguments->tcb;
	temptail=queue_info->tail;
	addNode(thread_block);
	
	/* HAVE TO ADD getttid here to assign the threadid to the current block */
	
	if(isfirsttime==0)
	{
		isfirsttime+=1;
		futex_up(&f);
	}
	else
	{
		futex_up(&block_main_futex);
	}
	
	//if(haveyielded>0)
	{
	//futex_down(&(temptail->thread->thread_futex));
	}
	futex_down(&(queue_info->tail->thread->thread_futex));//locking its own futex rather than that of the previous ones
	futex_down(&yield_futex);

	// this is for the join condition check if the current threads target is in the execution queue if yes the yield the current thread
	
	if(queue_info->head->thread->join_target==NULL)
	{
		jointargetcheck=0;
	}
	else// check if the target is present in the queue if yes yield
	{
		jointargetcheck=findthread(queue_info->head->thread->join_target);
	}
	if(jointargetcheck==1)//added the futex down statements today latest
	{
		jointargetcheck=0;
		futex_down(&(queue_info->head->thread->thread_futex));//locking its own futex rather than that of the previous ones
        	futex_down(&yield_futex);
		temp=mythread_join(*(queue_info->head->thread->join_target),status);
	}
	//jointargetcheck=mythread_join(queue_info->head->thread->join_target,status);
	
	(*(functionpointer))(((void *)arguments->functionarguments));
	
	// when this thread is done move the head to the next position and free the previous nodes futex
      	temphead=queue_info->head;
	queue_info->head=queue_info->head->right;
	if(queue_info->head!=NULL)
	 	queue_info->head->left=NULL;

       futex_up(&(queue_info->head->thread->thread_futex));//changing this 
       futex_up(&yield_futex);
       //futex_down(&(queue_info->tail->left->thread->thread_futex));
	total_threads=total_threads-1;
}

int mythread_create(mythread_t *new_thread_ID,
                    mythread_attr_t *attr,
                    void * (*start_func)(void *),
                    void *arg)
{
        mythread_t *mythread;
        struct wrapper_arguments *wrapperarguments;
        void* stack;
         pid_t pid;
         pid_t *childthreadid;
         int j=0;
         char ch;
	 char print_queue[20]="queue init\n";
         
	// Allocate the stack
         stack = malloc( FIBER_STACK );
	 mythread = (mythread_t *) malloc (sizeof(mythread_t));
	 mythread=new_thread_ID;
	 wrapperarguments = (struct wrapper_arguments *) malloc (sizeof(struct wrapper_arguments)); 	
	
	/* if mythread is called for the first time initialize the thread package */

	if (isfirsttime==0)
        {
		initialize_mythread_package();
	//isfirsttime2+=1;//new        
 	}
	else
	{
	//	futex_down(&f);
	}
	if ( stack == 0 )
         {
                 perror( "malloc: could not allocate stack" );
                exit( 1 );
         }
	 
 	 mythread->mystack = stack;
         mythread->myfunction =start_func;
         mythread->myarguments=arg;
         mythread->join_target=NULL;
	 mythread->myaddress=mythread;

	 wrapperarguments->functionname=start_func;
	 wrapperarguments->functionarguments=arg;
	 wrapperarguments->tcb=mythread;
	 futex_init(&(wrapperarguments->tcb->thread_futex),0);
	 void *wrapperarg = (void *) wrapperarguments;
        
        new_thread_ID-> tid = clone(&mythread_wrapper, (char*) stack + FIBER_STACK,
                CLONE_FS | CLONE_FILES | CLONE_SIGHAND | CLONE_VM,wrapperarg);
       	
	futex_down(&block_main_futex);
	//write(1,"thread created dude\n",21);
        return 0;
}

//key values start from 1 (key positions in global array starts from 0)
int mythread_key_create(mythread_key_t *key, void (*destructor)(void*)){
	int i;
	if(tsd_key_counter > MYTHREAD_KEYS_MAX)
	{	
		write(1, "\nmythread_key_create - EAGAIN: The system lacked the necessary resources to create another thread-specific data key, or the system-imposed limit on the total number of keys per process {PTHREAD_KEYS_MAX} has been exceeded.\n",225);
		return -1;
	}
	/*if key address already exists, return existing key address*/
	if(globalTSD != NULL)
	{
		for(i=0; i < tsd_key_counter; i++)
		{
			if(globalTSD[i].key == *key)
			{
				//navya: if key already exists what shud happen?
				write(1,"\nmythread_key_create - EINVAL: invalid argument. key already exists.\n",72);
				return -1;	
			}	
			if(globalTSD[i].key == 999)		
			{
				//if a key existed before and it was deleted, assign that key and don't increment tsd_key_counter
				*key = (mythread_key_t) (i+1);
				globalTSD[i].key = *key;
				globalTSD[i].ptr = NULL;
				return 0;
			}
		}
	}
	//if key does not exists, create a new key in the global array
	*key = (mythread_key_t) (tsd_key_counter+1);
	globalTSD[tsd_key_counter].key = *key;
	globalTSD[tsd_key_counter].ptr = NULL;			
	tsd_key_counter++;
	return 0;
}

int mythread_key_delete(mythread_key_t key)
{
	int j;
        for(j=0; j < tsd_key_counter; j++)
        {
                if(globalTSD[j].key == key)
                {
			globalTSD[j].key = 999;		//set key to some infinity value so that we know this key was deleted
			globalTSD[j].ptr = NULL;
                        //not decrementing counter here because a key in between the array is deleted/
			return 0;
                }
        }
	write(1,"\nmythread_key_delete - ENDENT: Key value is invalid/Key currently not allocated.\n",82);
	return -1;
}

void *mythread_getspecific(mythread_key_t key)
{
	int i;
	struct list_node* start;
	mythread_t tcb;
	const void *value;
	for(i=0; i< tsd_key_counter; i++)
	{
		if(globalTSD[i].key == key)
		{
			start = globalTSD[i].ptr;
			tcb = mythread_self();
			value = search_lnode(start, tcb);
			if(value == NULL)
			{	
                                write(1,"\nmythread_getspecific - Thread specific data value is not associated with the key",82);
                                return NULL;
                        }
			//printf("\nmythread_getspecific - Thread specific data value found with this key - %d", ((mythread_t *) value)->tid);
			return ((void *)value);
		}
	}
	//if key not found in the global array
	write(1, "\nmythread_getspecific - key not found in the global array\n",60);
	return NULL;
}

int mythread_setspecific(mythread_key_t key, const void *value)
{
        int i;
        lnode* start;
	lnode* new_lnode;
        int rc;
        mythread_t tcb;
	for(i=0; i < tsd_key_counter; i++)
        {
                if((globalTSD[i].key) == key)
                {
                        tcb = mythread_self();
			new_lnode = create_lnode(tcb, value);
                        if(globalTSD[i].ptr == NULL)
                        {
                                globalTSD[i].ptr = new_lnode;
				rc = 0;
                        }
                        else
                        {
                                start = globalTSD[i].ptr;
				/*passing tcb and value to check if a value already exists for this thread.
				*If it does, just change the value in that node and delete the new node
				*/
                                rc = append_lnode(start, new_lnode, /*key,*/ tcb, value);
                        }
                        if(rc == 0)
                        {
                                //printf("\nmythread_setspecific: value for this thread added successfully with key - %d\n",key);
                                return 0;
                        }
                        else if (rc == 1)
			{
				/*if the thread specific data value already existed,then it is updated to the new value*/
				printf("\nmythread_setspecific: value for this thread updated with key %d\n",key);
				return 0;
			}
			else
                        {
                                printf("\nmythread_setspecific - EINVAL: Invalid key. Failed to add with key %d\n",key);
                                return -1;
                        }
                }
        }
        write(1,"\nmythread_setspecific - EINVAL: Invalid key\n",45); 
        return -1;
}

void createmainthread()
{
	char print_queue[20]="main thread\n";
	void *stack;
	void *arg;
	node * mainthreadblock,*temptail;
	struct wrapper_arguments * wrapperarguments;
	stack = malloc( FIBER_STACK );
	write(1,print_queue,sizeof(print_queue));
	//total_threads+=1;
	if (stack == 0)
        {
                perror("malloc: could not allocate stack");
                exit(1);
        }
	arg=(void *) malloc(sizeof(void));
	mythread_t *mainthread=(mythread_t *) malloc (sizeof(mythread_t));
        mainthread->mystack = stack;
	mainthread->join_target=NULL;
        mainthread->myfunction =NULL;
        mainthread->myarguments=arg;
	mainthread->tid=0;
	futex_init(&(mainthread->thread_futex),0);

        mainthreadblock=createNode();
	temptail=(node *)malloc(sizeof(node));
        addNode(mainthreadblock);
	futex_init(&block_main_futex,0);
	ismainthreadcreated+=1;
	//futex_down(&block_main_futex);
}
