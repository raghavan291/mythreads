/* Single Author info: 
 * rnedunc Raghavendran Nedunchezhian
 * Group info:
 * lbalaku Lokheshvar Balakumar
 * rnedunc Raghavendran Nedunchezhian
 * nramanj Navya Ramanjulu */


/*
* list.h - This is the header file of the implementation of list
*/
#ifndef LIST_H_
#define LIST_H_
#include"mythread.h"

/* This structure is used to store thread specific data*/
typedef struct list_node
{
        mythread_t thread_control_block;
        const void *thread_value;
        struct list_node *link;
}lnode;

//These methods are used to access the list for thread specific data management
void *search_lnode(lnode* start, mythread_t tcb);
int delete_lnode(lnode* start, mythread_t tcb);
lnode* create_lnode(mythread_t tcb, const void *value);
int append_lnode(lnode* start,lnode* new_lnode, mythread_t tcb, const void *value);

#endif
