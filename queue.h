/*Single Author info: 
 * rnedunc Raghavendran Nedunchezhian
 * Group info:
 * lbalaku Lokheshvar Balakumar
 * rnedunc Raghavendran Nedunchezhian
 * nramanj	Navya Ramanjulu*/

/*
* queue.h - This is the header file of the queue implementation
*/

#ifndef QUEUE_H_
#define QUEUE_H_
#include "futex.h"
#include "mythread.h"

// The structure of the schedule queue

typedef struct queue_block
{
        mythread_t *thread;
        mythread_t *join_target;
	int node_pos;
        struct queue_block *left,*right;
}node;

// The global structure which the start,end and size of the queue

struct queue_main
{
        node *head;
        node *tail;
        int queue_size;
}*queue_info,*joinqueue;

// Scheduler queue specific functions
void init_queue_main();
void freeNode(node *current);
void display(node *current);
node* get_head();
node* get_tail();
int get_size_queue();
void thread_info_populate(mythread_t *thr);
node* createNode();
node* createjoinnode();
void addNode(node *newnode);
void addjoinnode(node *newnode);
int findthread(mythread_t *targetthread);
node* traverse(int thread_id);
void insertNode();
void deleteNode(node *current);


#endif
