/*Single Author info: 
 * rnedunc Raghavendran Nedunchezhian
 * Group info:
 * lbalaku Lokheshvar Balakumar
 * rnedunc Raghavendran Nedunchezhian
 * nramanj	Navya Ramanjulu*/
#include <stdio.h>
#include <stdlib.h>
#include "queue.h"
#include "mythread.h"

// This method initialises the queue for the first instance
void init_queue_main()
{
	queue_info = (struct queue_main*)malloc(sizeof(struct queue_main));	
	queue_info->queue_size=0;
	queue_info->head=NULL;
	queue_info->tail=NULL;

	joinqueue = (struct queue_main*)malloc(sizeof(struct queue_main));
        joinqueue->queue_size=0;
        joinqueue->head=NULL;
        joinqueue->tail=NULL;
}


// This method removes the thread control block from the scheduler queue
void freeNode(node *current)
{
	if(current==NULL)
	{
		//printf("The queue block is already empty");
		return;
	}
	free(current);
}


// This method is to display the contents of the thread control blocks present in the scheduler queue
void display(node *current)
{
	if(current == NULL)
	{
		//printf("Give queue block is empty");
		return;
	}
	node *temp;
	temp = current;
	do
	{
		//printf("Current node data is %d",temp->thread->tid);
		temp=temp->right;
	}while(temp!=NULL);
}

// This method returns the start node of the scheduler queue
node * get_head()
{
	return queue_info->head;
}

// This method returns the end node of the scheduler queue
node * get_tail()
{
	return queue_info->tail;
}

// This method returns the queue size of the scheduler queue
int get_size_queue()
{
	return queue_info->queue_size;
}

// This method creates thread control block for a thread
node* createNode()
{
	node *newnode;
	newnode = (node*)malloc(sizeof(node));
	newnode->left=NULL;
	newnode->right=NULL;
	newnode->thread=(mythread_t *)malloc(sizeof(mythread_t));
	(queue_info->queue_size)++;
	newnode->node_pos=queue_info->queue_size;	
	return newnode;
}

// This method adds the corresponding the thread control block into the scheduler queue
void addNode(node *newnode)
{
	if(queue_info->head==NULL)
	{
		queue_info->head=newnode;
		queue_info->tail=newnode;
		return;
	}
	newnode->left=queue_info->tail;
	queue_info->tail->right=newnode;
	queue_info->tail=newnode;
	newnode->right=NULL;
}

// This method is to yield the corresponding thread and perform the join operation   
void addjoinnode(node *newnode)
{

 	if(joinqueue->head==NULL)
  	{
   		joinqueue->head=newnode;
   		joinqueue->tail=newnode;
   		return;
  	}
  	newnode->left=joinqueue->tail;
  	joinqueue->tail->right=newnode;
  	joinqueue->tail=newnode; 
   	newnode->right=NULL;
} 

// This method is to create a temp node for join operation
node* createjoinnode()
{
        node *newnode;
        newnode = (node*)malloc(sizeof(node));
        newnode->left=NULL;
        newnode->right=NULL;
        newnode->thread=(mythread_t *)malloc(sizeof(mythread_t));
        (joinqueue->queue_size)++;
        newnode->node_pos=queue_info->queue_size;
        return newnode;
}

// This method is to find the thread in scheduler queue whose address is passed as function arguments
int findthread (mythread_t *targetthread)
{
	node *temp = queue_info->head;
	while(temp!=NULL)
	{
		//printf("source %d : target %d \n",temp->thread->tid,targetthread->tid);
		if(temp->thread->tid==targetthread->tid)
			return 1;	
		else
		{
			temp=temp->right;
		}
         }	
	return 0;
}

// This method is to traverse through the scheduler queue with corresponding thread ID and return thats corresponding thread control block
node* traverse(int thread_id)
{
	node *temp = queue_info->head;
	while((temp->thread->tid)!=(thread_id))
	{
		temp=temp->right;
	}
	return temp;
}

// This method is to remove the thread control block from the scheduler amd frees its memory
void deleteNode(node *current)
{
	if(queue_info->head==NULL)
	{
		write(1,"Queue is empty",15);
		return;
	}
	else if(queue_info->head==queue_info->tail)
	{
		
	}
	else if (current->left==NULL)
	{
		queue_info->head=current->right;
		queue_info->head->left=NULL;
	}
	else
	{
		current->left->right = current->right;
		current->right->left = current->left;
	}
	(queue_info->queue_size)--;	
	freeNode(current);
}
